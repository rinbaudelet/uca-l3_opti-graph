from sage.graphs.digraph import DiGraph

flowgraph = DiGraph(15)
flowgraph.add_edges([[0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [1, 6], [1, 7], [2, 7],
                     [2, 8], [3, 8], [3, 9], [4, 9], [4, 10], [5, 10], [5, 6],
                     [6, 11], [7, 11], [8, 12], [9, 13], [10, 13], [13, 12],
                     [12, 11], [11, 14], [12, 14], [13, 14]])

flowgraph.set_edge_label(0, 1, 1)
flowgraph.set_edge_label(0, 2, 7)
flowgraph.set_edge_label(0, 3, 2)
flowgraph.set_edge_label(0, 4, 5)
flowgraph.set_edge_label(0, 5, 10)
flowgraph.set_edge_label(5, 6, 1)
flowgraph.set_edge_label(1, 6, 1)
flowgraph.set_edge_label(1, 7, 2)
flowgraph.set_edge_label(2, 7, 3)
flowgraph.set_edge_label(2, 8, 5)
flowgraph.set_edge_label(3, 8, 42)
flowgraph.set_edge_label(3, 9, 17)
flowgraph.set_edge_label(4, 9, 21)
flowgraph.set_edge_label(4, 10, 2)
flowgraph.set_edge_label(5, 10, 27)
flowgraph.set_edge_label(6, 11, 2)
flowgraph.set_edge_label(7, 11, 1)
flowgraph.set_edge_label(8, 12, 2)
flowgraph.set_edge_label(9, 13, 1)
flowgraph.set_edge_label(10, 13, 2)
flowgraph.set_edge_label(13, 12, 21)
flowgraph.set_edge_label(12, 11, 21)
flowgraph.set_edge_label(11, 14, 4)
flowgraph.set_edge_label(12, 14, 1)
flowgraph.set_edge_label(13, 14, 1)

figure_1 = {
    's': {'a': 1, 'b': 7, 'c': 2, 'd': 5, 'e': 10},
    'a': {'f': 1, 'g': 2},
    'b': {'g': 3, 'h': 5},
    'c': {'h': 42, 'i': 17},
    'd': {'i': 21, 'j': 2},
    'e': {'j': 27, 'f': 1},
    'f': {'k': 2},
    'g': {'k': 1},
    'h': {'l': 2},
    'i': {'m': 1},
    'j': {'m': 2},
    'k': {'t': 4},
    'l': {'k': 21, 't': 2},
    'm': {'l': 21, 't': 1}
}
figure_1 = DiGraph(figure_1, format='dict_of_dicts')

figure_2 = {
    'a': ['b', 'c', 'd'],
    'b': ['a', 'c', 'd'],
    'c': ['b'],
    'd': ['a']

}
figure_2 = DiGraph(figure_2, format='dict_of_lists')
