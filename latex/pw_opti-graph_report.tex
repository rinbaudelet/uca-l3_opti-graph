\documentclass[12pt,a4paper]{article}

\usepackage[margin=2.5cm]{geometry}
\usepackage{tocloft}
\usepackage{amsfonts}
\usepackage{array}
\usepackage{multirow}
\usepackage{makecell}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{lettrine}
\usepackage{lipsum} 
\usepackage{circuitikz}
\usepackage{amsmath,scalerel}
\usepackage{xcolor}
\usepackage{enumitem}
\usepackage[hidelinks,pdftex,
            pdfauthor={Rin Baudelet, Léna Combronde},
            pdftitle={Problèmes de Flots - Travaux Pratiques},
            pdfsubject={},
            pdfproducer={Latex with hyperref},
            pdfcreator={pdflatex}]{hyperref}
\usepackage[document]{ragged2e}
\usepackage{tcolorbox}
\usepackage{fancyhdr}
\usepackage[Glenn]{fncychap}
\usepackage{lastpage}
\usepackage{pgfplots}
\usepackage{graphicx}
\usepackage{glossaries}
\usepackage{blkarray}
\pgfplotsset{compat=1.17}
\graphicspath{ {./images/} }

\setcellgapes{1pt}
\makegapedcells
\newcolumntype{R}[1]{>{\raggedleft\arraybackslash }b{#1}}
\newcolumntype{L}[1]{>{\raggedright\arraybackslash }b{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash }b{#1}}

\fancypagestyle{basic}{
	\fancyhf{}
	\renewcommand{\headrulewidth}{0pt}
	\cfoot{}
}

\fancypagestyle{theannex}{
	\fancyhf{}
	\lhead{TP - Problèmes de flots}
	\rhead{R. Baudelet, L. Toumi}
	\chead{\textbf{Annexe}}
	\cfoot{Page \thepage \;sur \pageref{LastPage}}
}

\pagestyle{fancy}
\fancyhf{}
\rhead{R. Baudelet, L. Combronde}
\lhead{TP - Problèmes de flots}
\cfoot{Page \thepage \;sur \pageref{LastPage}}

\title{TP - Problèmes de flots\\\small{2022/2023}}
\author{Rin Baudelet, Léna Combronde}
\date{\today}
\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}
\newcommand{\tabs}[2]{\hspace*{#2} #1 \hspace*{#2}}
\newcommand{\citeb}[1]{[\cite{#1}]}

\newcommand{\jmpP}[0]{\vspace*{6mm}}
\newcommand{\jmpp}[0]{\vspace*{4mm}}
\newcommand{\jmpi}[0]{\vspace*{2mm}}
\newcommand{\midpnt}{·}
\newcommand{\card}[1]{\vert #1 \vert}
\newcommand{\dbleunderline}[1]{\underline{\underline{#1}}}

\makenoidxglossaries
\renewcommand*\glspostdescription{\dotfill}

\newglossaryentry{gls_template}
{
	name=something,
	description={the description}
}


\begin{document}
\maketitle 
\vfill
\tableofcontents
\vfill
\begin{center}

\href{http://creativecommons.org/licenses/by-nc-sa/4.0/}{\includegraphics{88x31.png}}
\vspace*{3mm} \\ {\color{blue} \underline{ \url{https://gitlab.isima.fr/rinbaudelet/uca-l3_opti-graph}}}
\end{center}
\thispagestyle{basic}
\pagebreak
%
%%% CONTENTS 
%
\section{Projet: Documentation}
\begin{flushleft}
	Le projet a été organisé avec l'arborescence suivante:
	\begin{itemize}[label=$\bullet$]
		\item la \textbf{racine} du projet contient le fichier principal du programme 'main.py' ainsi que les quelques fichiers de configurations du projet (README, fichier(s) git, requirements.txt...) 
		\item le dossier/package \textbf{models} contenant l'implémentation des figures du projet (cf. consignes du TP).
		\item le dossier \textbf{notebooks} contenant les fichiers Jupyter pour tester les fonctions avec SageMath, affichage des résultats$\ldots$.
		\item le dossier \LaTeX{} pour la rédaction de ce rapport.
		\item le package \textbf{ucagraphs} contient les outils programmés pour ce TP, et constitue le coeur du projet. 
	\end{itemize}
\end{flushleft}
\subsection{Packages}
\begin{itemize}[label=]
	\item \textbf{ucagraphs} \jmpi\\
		Le package \textbf{ucagraphs} contient les fonctions classiques programmées dans le cadre de ces travaux pratiques. Il importe également le package \textbf{flowing} décrit ci-dessous. 
	\item \textbf{ucagraphs.flowing} \jmpi\\
		Le package \textbf{ucagraphs.flowing} contient les classes de gestion des structures de graphes. Il contient notamment deux classes \textit{FlowingGraph} et \textit{ResidualGraph}. 
\end{itemize}
\subsection{Classes}
\begin{itemize}[label=]
	\item \textbf{FlowingGraph}(DiGraph) \jmpi \\
		Décrit un graphe de flux, à savoir un graphe orienté ayant une source et un puit. C'est-à-dire un sommet avec uniquement des arètes sortantes (resp. rentrantes), et pour chaque arète (u, v), il n'existe pas d'arète (v, u). Chaque arète possède une capacité $c > 0$.
	\item \textbf{ResidualGraph}(FlowingGraph) \jmpi \\
		Décrit un graphe de flux particulier obtenu à partir d'un graphe de flux, à capacité ou à flux (flow/capacité), telle que pour chaque arète ($u, v$), il existe son chemin retour ($v, u$) dont on aura calculé la capacité à partir de celle initiale (ou du flux initial). \jmpi\\
		Cette structure est tout spécialement utilisée notamment pour calculer à partir d'un graphe de flux (\textit{FlowingGraph}), un flux maximum en utilisant l'algorithme d'Edmonds-Karp à partir de l'algorithme de parcours en largeur, révisé pour le graphe résiduel. 
\end{itemize}
\section{Algorithme d'Edmonds-Karp}
\subsection{Breadth First Search de graphe orienté}
\begin{flushleft}
	\begin{tcolorbox}[title={Algorithme de BFS orienté}]
	\begin{itemize}[label=$\bullet$]
		\item Input: $D(V,A)$ un graphe orienté, \\
		\hspace*{12mm} $s$, un sommet 
		
		\item Output: $\sigma : V \rightarrow \mathbb{N}$, permutation des sommets \\
		\hspace*{15mm} $T$: un arbre couvrant enraciné en $s$
	\end{itemize}
	\tcblower
	\begin{justify}
		Foreach $v \in V(D)$ Do \\
		\begin{tabular}{|L{1mm} l}
			& $l\left[v\right]= +\infty$ {\color{darkgray} // niveau} \\
			& $p\left[v\right]= \emptyset$ {\color{darkgray} // parent} \\
			& $\sigma\left[v\right]= -1$ {\color{darkgray} // n° sommet} \\
			& $c\left[v\right]=$ blanc {\color{darkgray} // couleur} \\
			\cline{1-1}
		\end{tabular} \jmpi \\
		$L$ := $\emptyset$ \\
		$l\left[s\right]$ := $0$ \\
		Append($L,\{s\}$) \\
		$i=1$ \\
		While ($L \neq \emptyset$) Do \\ 
		\begin{tabular}{|L{1mm} l}
			& $v$ := FirstElement($L$) \\
			& $L$ := $L \setminus \{v\}$ \\ 
			& $\sigma\left[v\right]$ := $i$ \\
			& $i++$ \\
			& Foreach($w \in N^+(v)$ with $c\left[w\right] == $ blanc) Do \\
			& \begin{tabular}{|L{1mm} l} 
				& Append($L$, $w$) \\
				& $c\left[w\right]$ := gris \\
				& $P\left[w\right]$ := $v$ \\
				& $l\left[w\right]$ := $l\left[v\right] + 1$ \\
				\cline{1-1} 
			  \end{tabular} \\
			& $v := 0$ \\
			& If ($L==\emptyset$) Then \\
			& \begin{tabular}{|L{1mm} l}
				& Foreach($v' \in A$) Do \\
				& \begin{tabular}{|L{1mm} l}
					& If ($c[v'] ==$ blanc) Then \\
					& \begin{tabular}{|L{1mm} l}
						& $v$ := $v'$ \\
						& Break\\
					\end{tabular} \\
				\end{tabular} \\ 
				& If ($v \neq$ 0) Then \\
				& \begin{tabular}{|L{1mm} l}
					& Append($L, v$) \\ \cline{1-1}
				\end{tabular} \\ \cline{1-1}
			  \end{tabular} \\
			\cline{1-1}
		\end{tabular} \jmpi \\
		Return $\sigma$, $T$
	\end{justify}
	\end{tcolorbox}
\end{flushleft}
\subsection{Gestion d'un graphe résiduel}
\begin{justify}
	Pour gérer les graphes résiduels, nous utiliserons une classe \textit{ResidualGraph} qui se construit à partir d'un graphe parent de type \textit{FlowingGraph}. Durant la construction, le graphe construira pour chaque arète $(u, v)$ du père, une arète $(v, u)$ qui sera:
	\begin{itemize}[label=-]
		\item de poid nul si les arètes du graphe initial ont des valeurs entières en label
		\item d'un poid qu'on calculera selon les valeurs $\frac{\text{flux}}{\text{capacité}}$
	\end{itemize} 
	\jmpi
	\noindent
	La classe \textit{ResidualGraph} possède des fonctions utiles dont notamment la fonction de parcours en largeur utilisée pour l'algorithme d'Edmonds-Karp ainsi qu'une fonction pour déterminer, au fur et à mesure des changements opérés, si le puit est atteignable depuis une source $s$. 
\end{justify}
\subsection{Implémentation et résultats}
\begin{justify}
	\begin{figure}[h]
		\centering
		\begin{tikzpicture}[scale=0.75]
			\begin{scope}[every node/.style={circle,fill=blue!20,thick,draw}]
				\node (s) at (0,3) {s};

				\node (a) at (3,6) {a};
				\node (b) at (3,4.5) {b};
				\node (c) at (3,3) {c};
				\node (d) at (3,1.5) {d};
				\node (e) at (3,0) {e}; 
				
				\node (f) at (6,6) {f};
				\node (g) at (6,4.5) {g};
				\node (h) at (6,3) {h};
				\node (i) at (6,1.5) {i};
				\node (j) at (6,0) {j};
				
				\node (k) at (9,4.5) {k};
				\node (l) at (9,3) {l};
				\node (m) at (9,1.5) {m};
				
				\node (t) at (12,3) {t};
			\end{scope}
			\begin{scope}[>={Stealth[black]},
							every node/.style={above},
							every edge/.style={draw=black}]
				\path[->] (s) edge node {$1$} (a);
				\path[->] (s) edge node {$7$} (b);
				\path[->] (s) edge node {$2$} (c);
				\path[->] (s) edge node {$5$} (d);
				\path[->] (s) edge node {$10$} (e); 
				\path[->] (a) edge node {$1$} (f);
				\path[->] (a) edge node {$2$} (g);
				\path[->] (b) edge node {$3$} (g);
				\path[->] (b) edge node {$5$} (h);
				\path[->] (c) edge node {$42$} (h);
				\path[->] (c) edge node {$17$} (i);
				\path[->] (d) edge node {$21$} (i);
				\path[->] (d) edge node {$2$} (j);
				\path[->] (e) edge node {$27$} (j);
				\draw[->] (e) to [out=-180, in=-90] (-0.5, 3) to [out=90, in=145]
						node {$1$} (f);
				\path[->] (f) edge node {$2$} (k);
				\path[->] (g) edge node {$1$} (k);
				\path[->] (h) edge node {$2$} (l);
				\path[->] (i) edge node {$1$} (m);
				\path[->] (j) edge node {$2$} (m);
				\path[->] (k) edge node {$4$} (t);
				\path[->] (l) edge node[left] {$21$} (k);
				\path[->] (l) edge node {$2$} (t);
				\path[->] (m) edge node[left] {$21$} (l);
				\path[->] (m) edge node {$1$} (t);
			\end{scope}
		\end{tikzpicture}
		\caption{Exemple de graphe de flot utilisé pour l'exploitation d'Edmonds-Karp}
	\end{figure}
	On implémente la figure ci-dessus dans le package \textit{models} pour exécuter l'algorithme d'Edmonds-Karp. On obtient un flot maximum de $7$. Et on obtient le graphe de flot maximum suivant : \\
	\begin{figure}[h]\label{fig:max_flow_graph}
		\centering
		\includegraphics[scale=0.75]{figure_1.2.png}
		\caption{Graphe de flot maximum obtenu par Edmonds-Karp sur la figure 1}
	\end{figure}
\end{justify}
\pagebreak
\section{Problème: séquence de degré d'un graphe orienté}
\begin{justify}
	\begin{figure}[h]\label{fig:figure_2}
		\centering
		\begin{tikzpicture}
			\begin{scope}[every node/.style={circle,draw,thick,fill=blue!20}]
				\node (a) at (0,3) {a};
				\node (b) at (0,0) {b};
				\node (c) at (3,0) {c};
				\node (d) at (3,3) {d};
			\end{scope}
			\begin{scope}[>={Stealth[black]},
							every node/.style={above},
							every edge/.style={draw=black}]
				\path[->] (a) edge (d);
				\path[->] (a) edge[bend right] (b);
				\path[->] (a) edge (c);
				\path[->] (b) edge (a);
				\path[->] (b) edge (d);
				\path[->] (b) edge (c);
				\path[->] (c) edge[bend left] (b);
				\path[->] (d) edge[bend right] (a);
			\end{scope}
		\end{tikzpicture}
		\caption{Un graphe qui réalise la séquence de degrés $\{(3,2), (3,2), (1,2), (1,2)\}$}
	\end{figure}

Dans un premier temps on regarde si l'on peut créer un graphe orienté qui réalise cette séquence.Pour cela, on regarde si notre
séquence contient pas un nombre plus grand ou égal au nombre de sommets, car un sommet peut-être au maximum connecté à tous les autres. De plus,
on regarde si la somme des degrès entrants est égale à la somme des degrès sortant car il est impossible que le flot sortant de la source soit 
différent de celui qui entre dans le puit. Une fois toute ces vérifications faites, nous pouvons créer un graphe correspodant à la séquence. Pour 
cela, nous commencons par modéliser le problème en un problème de flot maximum. On crée donc un digraph avec 10 sommets parmis lesquels l'un sera la 
source et un autre, le puit. Puis les 8 autres sommets seront les sommets de la séquence deux fois reproduits. On fait donc partir de la source des arètes
qui vont jusqu'aux sommets d'une des deux séquences et l'on met sur ces arètes les labels qui correspondent au degrès sortant du sommet en question  
dans la séquence, puis on fait partir de l'autre séquence des arètes qui vont jusqu'au puit, avec comme label celui du degrès sortant du sommet 
en question dans la séquence. Puis entre chaque sommet des 2 séquences, on crée des arètes qui relient chaque sommet de la séquence à chaque sommet de l'autre séquence, sauf si le sommet en question est identique et on met un label de $1$ sur toutes ces arètes.

	\begin{figure}[h]
		\centering
		\includegraphics[scale=0.7]{figure_3.png}
		\caption{Graphe de flot obtenu en transformant la séquence en graphe de problème de flots}
	\end{figure}

\noindent
Une fois le problème modélisé en un problème de flot, on fait appel à l'algorithme d'Edmonds-Karp qui va retourner le flot maximum du graphe que l'on vient de créer grâce à la séquence.
	\begin{figure}[h]\label{fig:figure_4}
		\centering
		\includegraphics[scale=0.7]{figure_4.png}
		\caption{Graphe obtenu après transformation du graphe de flot maximum}
	\end{figure}

\noindent
En comparant le graphe ci-dessus avec le graphe donné en exemple (cf. figure 2 p.\ref{fig:figure_2}), on peut noter que les deux graphes sont similaires et qu'il réalise bien la séquence donné. On a pu ainsi montrer que notre modélisation du problème en problème de flot maximum est correct et répond à la problématique posée. 
\end{justify}
\end{document}
