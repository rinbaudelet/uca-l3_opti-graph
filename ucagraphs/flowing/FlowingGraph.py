from typing import Any
from random import randint

from sage.graphs.digraph import DiGraph
from sage.graphs.digraph_generators import digraphs
from sage.rings.integer import Integer

from ucagraphs.flowing.ResidualGraph import ResidualGraph


class FlowingGraph(DiGraph):
    """
    A Flowing Graph is a DiGraph containing a source and well's vertices with
    capacity as label, meaning that the graph is weighted.
    """

    def __generate_random_digraph(self, n: int, p: float,
                                  min_capacity: int, max_capacity: int):
        """
            Generates a random flowing graph

            :param n: The number of vertices to put in the graph
            :param p: Probability of connection between vertices (excluding well
                      and source)
            :param min_capacity: The minimum weight of an edge
            :param max_capacity: The maximum weight of an edge
            """
        if n < 5:
            raise Exception("Amount of vertices need to be higher than 5 to be "
                            "properly done!")
        self.__init__(digraphs.RandomDirectedGNP(n, p))

        well_potential_dict = {len(list(self.neighbors_in(vertex))): vertex
                               for vertex in self.vertices()}
        src_potential_dict = {len(list(self.neighbors_out(vertex))): vertex
                              for vertex in self.vertices()}

        well_attach_vertex = well_potential_dict[max(well_potential_dict.keys())]

        edge_count = max(src_potential_dict.keys())
        src_attach_vertex = src_potential_dict[edge_count]
        while src_attach_vertex == well_attach_vertex:
            del src_potential_dict[edge_count]
            edge_count = max(src_potential_dict.keys())
            src_attach_vertex = src_potential_dict[edge_count]

        self.add_vertex('S')
        self.add_vertex('W')
        self.add_edge('S', src_attach_vertex)
        self.add_edge(well_attach_vertex, 'W')
        for u, v, _ in self.edges(sort=False):
            capacity = randint(min_capacity, max_capacity)
            self.set_edge_label(u, v, (randint(0, capacity), capacity))
            if (v, u) in self.edges(sort=False, labels=False):
                self.delete_edge(v, u)
        self._src_vertex = 'S'
        self._well_vertex = 'W'

    def __check_digraph(self) -> bool:
        """
        Checks if the current graph is a valid flowing graph. It means that
        every edge (u, v) doesn't have its other way (v, u) plus each edge
        got an integer value as label.
        :return: True if the graph is valid, false otherwise
        """
        for (u, v, label) in self.edges(labels=True, sort=False):
            if (v, u) in self.edges(labels=False, sort=False):
                return False
            if not isinstance(label, int) and not isinstance(label, Integer):
                return False
        return True

    def __init__(self, generate: Any = 5, p: float = .4,
                 min_capacity: int = 5, max_capacity: int = 50, src_vertex=0,
                 well_vertex=None):
        """
        Create a new Flowing Graph.
        :param generate: the graph generator. If generate is an int, the flowing
        graph will be randomly generated with generate + 2 vertices. If generate is
        a DiGraph, make a copy of it and make sure that the DiGraph is indeed a
        flowing graph (got a well and source graph).
        :param p: the connector's factor (0 < p <= 1)
        :param min_capacity: the minimum capacity for an edge during generation
        :param max_capacity: the maximum capacity for an edge during generation
        :param src_vertex: the source's vertex, ignored during random generation
        :param well_vertex: the well's vertex, ignored during random generation
        """
        if type(generate) == int:
            super().__init__(digraphs.RandomDirectedGNP(generate, p))
            self.__generate_random_digraph(generate, p, min_capacity, max_capacity)
        elif type(generate) == FlowingGraph:
            super().__init__(generate)
        elif type(generate) == DiGraph:
            super().__init__(generate)
            if not self.__check_digraph():
                raise Exception("the passing graph contains a dual direction "
                                "edge and cannot be converted to a valid flowing "
                                "graph")
            self._src_vertex = src_vertex
            self._well_vertex = well_vertex if well_vertex is not None else \
                self.vertices()[len(self.vertices()) - 1]
        else:
            raise Exception(f"Invalid generate value passed in arguments ! ")

    @property
    def src_vertex(self):
        return self._src_vertex

    @property
    def well_vertex(self):
        return self._well_vertex

    def zero(self):
        """
        Put every edge's labels to 0
        """
        for u, v in self.edges(labels=False, sort=False):
            self.set_edge_label(u, v, 0)

    def get_residual(self) -> ResidualGraph:
        return ResidualGraph(self)

    def well_reachable(self, src=None) -> bool:
        """
        Determine if the well is reachable from the vertex 'src'
        :param src: The source vertex. If is None, src equals the graph's source
        :return: True if the well is accessible from the vertex 'src', False otherwise
        """
        return self.get_residual().well_reachable(src)

    def get_bfs_tree(self, start=None) -> DiGraph:
        """

        :param start:
        :return:
        """
        if start is None:
            start = self.src_vertex

        bfs = DiGraph()
        c: dict = {vertex: 0 for vertex in self.vertices()}

        c[start] = 1
        remains_vertex = [start]

        def get_unvisited_vertex():
            for vertex in self.vertices():
                if c[vertex] == 0:
                    return vertex
            return None

        while len(remains_vertex) != 0:
            v = remains_vertex[0]
            remains_vertex.remove(v)
            for neighbor in self.neighbors_out(v):
                capacity = self.edge_label(v, neighbor)
                if c[neighbor] == 0 and capacity > 0:
                    remains_vertex.append(neighbor)
                    bfs.add_edge(v, neighbor)
                    c[neighbor] = 1

            if len(remains_vertex) == 0:
                v = get_unvisited_vertex()
                if v is not None:
                    remains_vertex.append(v)
        return bfs

    def get_maximal_flow(self):
        """
        Get the maximal flow of the flowing graph
        :return: (int, ResidualGraph, FlowingGraph)
        """
        residual_g = self.get_residual()
        flow_g = FlowingGraph(self)
        flow_g.zero()

        def path_to_well(tree: DiGraph):
            """
            Get a path to the well from the source using a BFS tree
            :param tree: A BFS Tree
            :return: a list of edges describing the path from the source vertex to
            the well.
            """
            path = []
            min_capacity = 0x7fffffff

            current = self.well_vertex
            neighbour = None
            while neighbour != self.src_vertex:
                neighbour = tree.neighbors_in(current)[0]
                path.insert(0, (neighbour, current))
                min_capacity = min(min_capacity, residual_g.edge_label(neighbour,
                                                                       current))
                current = neighbour

            return path, min_capacity

        flow = 0
        while residual_g.well_reachable(self.src_vertex):
            bfs_tree = residual_g.get_bfs_tree(self.src_vertex)
            edges, capacity = path_to_well(bfs_tree)
            flow += capacity
            for (u, v) in edges:
                is_original_edge = (u, v) in flow_g.edges(labels=False, sort=False)
                edge_label_uv = residual_g.edge_label(u, v)
                edge_label_vu = residual_g.edge_label(v, u)
                if is_original_edge:
                    label = flow_g.edge_label(u, v)
                    flow_g.set_edge_label(u, v, label+capacity)
                    residual_g.set_edge_label(u, v, edge_label_uv - capacity)
                    residual_g.set_edge_label(v, u, edge_label_vu + capacity)
                else:
                    label = flow_g.edge_label(v, u)
                    flow_g.set_edge_label(v, u, label-capacity)
                    residual_g.set_edge_label(u, v, edge_label_uv + capacity)
                    residual_g.set_edge_label(v, u, edge_label_vu - capacity)
        return flow, residual_g, flow_g
