from sage.graphs.digraph import DiGraph
from sage.rings.integer import Integer


class ResidualGraph(DiGraph):
    """
    A Residual Graph is a DiGraph with, for each edge (u, v) with a weight 'w',
    the edge (v, u) exists with a weight m.

    A Residual Graph can be built from a Flowing Graph, with for each edge (u, v),
    an edge (v, u) is created with weight = 0. Otherwise, from a RatedWeightedGraph
    with each weight computed according to their flow/capacity.

    The residual graph is maily used by FlowingGraph to compute the maximum flow
    using the Edmonds-Karp Algorithm.
    """

    def __init__(self, parent):
        """
        Create a Residual Graph from another graph 'parent'.

        :param parent: the parent graph. Can be a Flowing Graph or a
        RatedWeightedGraph
        """
        if parent is None:
            raise Exception("parent cannot be None")
        super().__init__(parent)
        self._parent = parent

        for u, v, label in self.parent.edges(sort=False):
            if type(label) == int or type(label) == Integer:
                self.add_edge(v, u, 0)
            else:
                flow, capacity = label
                if flow < capacity:
                    self.set_edge_label(u, v, capacity - flow)
                    self.add_edge(v, u, flow)
                elif flow == capacity:
                    self.delete_edge(u, v)

    @property
    def parent(self):
        return self._parent

    @property
    def well_vertex(self):
        return self.parent.well_vertex

    @property
    def src_vertex(self):
        return self.parent.src_vertex

    def well_reachable(self, src) -> bool:
        return self.well_vertex in self.get_bfs_tree(src).vertices(sort=False)

    def get_bfs_tree(self, src=None) -> DiGraph:
        """
        Get the connex BFS tree from the vertex 'src' regarding the edge's capacity
        :param src: the root vertex to start the BFS from.
        :return: A connex BFS tree (ignoring inaccessible vertices)
        """
        if src is None:
            src = self.src_vertex

        bfs = DiGraph()
        c: dict = {vertex: 0 for vertex in self.vertices()}

        c[src] = 1
        remains_vertex = [src]

        while len(remains_vertex) != 0:
            v = remains_vertex.pop(0)
            for neighbor in self.neighbors_out(v):
                capacity = self.edge_label(v, neighbor)
                if c[neighbor] == 0 and capacity > 0:
                    remains_vertex.append(neighbor)
                    bfs.add_edge(v, neighbor)
                    c[neighbor] = 1

        return bfs
