import random

from sage.graphs.digraph import DiGraph
from sage.graphs.graph import Graph

from ucagraphs.flowing import ResidualGraph, FlowingGraph


# noinspection PyPep8Naming
def BFS(g: DiGraph, s: int) -> DiGraph:
    """

    :param g:
    :param s:
    :return:
    """
    bfs = DiGraph(len(g.vertices()))
    c: dict = {vertex: 0 for vertex in g.vertices()}

    c[s] = 1
    remains_vertex = [s]

    def get_unvisited_vertex():
        for vertex in g.vertices():
            if c[vertex] == 0:
                return vertex
        return None

    while len(remains_vertex) != 0:
        v = remains_vertex[0]
        remains_vertex.remove(v)
        for neighbor in g.neighbors_out(v):
            if c[neighbor] == 0:
                remains_vertex.append(neighbor)
                bfs.add_edge(v, neighbor)
                c[neighbor] = 1

        v = get_unvisited_vertex()
        if len(remains_vertex) == 0 and v is not None:
            remains_vertex.append(v)

    return bfs


def is_eulerian(g: Graph) -> bool:
    for d in g.degree():
        if d == 0 or d % 2 == 1:
            return False
    return True


def random_flowgraph(n: int, p: float = 0.5, min_value: int = 5,
                     max_value: int = 50, include_flow: bool = True) \
        -> DiGraph:
    """
    Create a random capacity or flow graph

    :param n: The number of vertices to put in the graph
    :param p: Probability of connection between vertices (excluding well and source)
    :param min_value: The minimum weight of an edge
    :param max_value: The maximum weight of an edge
    :param include_flow: If true, the returned graph will be a flow graph
    :return: A Random flowgraph with 'S' as the source, 'W' as the well
    """
    from sage.graphs.digraph_generators import digraphs

    if n < 5:
        raise Exception("Amount of vertices need to be higher than 5 to be "
                        "properly done!")

    digraph: DiGraph = digraphs.RandomDirectedGNP(n, p)
    well_potential_dict: dict = {len(list(digraph.neighbors_in(vertex))): vertex
                                 for vertex in digraph.vertices()}
    src_potential_dict: dict = {len(list(digraph.neighbors_out(vertex))): vertex
                                for vertex in digraph.vertices()}

    well_attach_vertex = well_potential_dict[max(well_potential_dict.keys())]

    edge_count = max(src_potential_dict.keys())
    src_attach_vertex = src_potential_dict[edge_count]
    while src_attach_vertex == well_attach_vertex:
        del src_potential_dict[edge_count]
        edge_count = max(src_potential_dict.keys())
        src_attach_vertex = src_potential_dict[edge_count]

    digraph.add_vertex('S')
    digraph.add_vertex('W')
    digraph.add_edge('S', src_attach_vertex)
    digraph.add_edge(well_attach_vertex, 'W')
    for u, v, _ in digraph.edges(sort=False):
        capacity = random.randint(min_value, max_value)
        digraph.set_edge_label(u, v, (random.randint(0, capacity), capacity)
        if include_flow else capacity)

    return digraph


def residual(flow_g: DiGraph) -> DiGraph:
    residual_g = flow_g.copy()
    for u, v, (flow, capacity) in flow_g.edges(sort=False):
        if flow < capacity:
            residual_g.set_edge_label(u, v, capacity - flow)
            residual_g.add_edge(v, u)
            residual_g.set_edge_label(v, u, flow)
        elif flow == capacity:
            residual_g.delete_edge(u, v)
    return residual_g


def degrees_to_graph(sequence) -> (DiGraph, DiGraph):
    """
    Transforme une séquence de degrées en un graphe qui réalise la séquence donnée.
    :param sequence: La séquence de degrée sous forme de liste de tuples contenant
    deux entiers (out, int). La transformation se base sur la modélisation de la
    séquence en un problème de flot maximum.
    :return: Un graphe réalisant la séquence de degrées.
    """
    lg_sequence = len(sequence)
    sum_degres_sortant = 0
    sum_degres_entrant = 0
    for i in sequence:
        if i[0] >= lg_sequence or i[1] >= lg_sequence:
            raise Exception("impossible de créer un graphe orienté qui réalise cette "
                    "séquence")
        sum_degres_sortant += i[0]
        sum_degres_entrant += i[1]

    # la capacité du puit et de la source n'est pas la meme donc impossible
    if sum_degres_sortant != sum_degres_entrant:
        raise Exception("impossible de créer un graphe orienté qui réalise cette "
                    "séquence")
    else:  # On peut créer le graphe
        graphe_de_flot = DiGraph(lg_sequence * 2 + 2)
        num = 0
        for i in range(1, lg_sequence + 1):
            graphe_de_flot.add_edges(
                [(0, i)])  # crée les aretes et labels de la source au nb de somments
            graphe_de_flot.set_edge_label(0, i, sequence[num][0])
            num += 1

        num = 0
        for i in range(lg_sequence + 1, ((lg_sequence * 2 + 2) - 1)):
            # créer les arètes et labels des sommets au puit
            graphe_de_flot.add_edges([(i, (
                    lg_sequence * 2 + 2 - 1))])
            graphe_de_flot.set_edge_label(i, (lg_sequence * 2 + 2 - 1),
                                          sequence[num][1])
            num += 1

        for i in range(1, lg_sequence + 1):
            for j in range(lg_sequence + 1, (lg_sequence * 2 + 2) - 1):
                if j != i + lg_sequence:
                    graphe_de_flot.add_edges([(i, j)])
                    graphe_de_flot.set_edge_label(i, j, 1)
                    # A partir de là, on a modelisez le problème en un problème de
                    # flot maximum

        _, _, graphe_flot_max = FlowingGraph(graphe_de_flot).get_maximal_flow()

        # on va maintenant faire le graphe à partir du graphe de flot maximum
        # obtenu grâce à l'algorithme d'Edmonds-Karp
        graphe_flot_max.delete_vertex(len(graphe_flot_max.vertices()) - 1)
        graphe_flot_max.delete_vertex(0)
        for vertex_u, vertex_v, edge_flow in graphe_flot_max.edges():
            if edge_flow == 0:
                graphe_flot_max.delete_edge(vertex_u, vertex_v)

        def transformation(graph):
            """
            Permet de transformer le graphe de flot maximum en graphe de séquence
            :param graph: Le graphe de flot maximum.
            :return: Le graphe final de la séquence.
            """
            original_vertices = graph.vertices()
            final_graph = DiGraph(len(original_vertices) // 2)
            for u in range(0, len(original_vertices) // 2):
                for v in range(len(original_vertices) // 2, len(original_vertices)):
                    if (u + 1, v + 1) in graph.edges(labels=False, sort=False):
                        final_graph.add_edges([(u, v % len(final_graph.vertices()))])
            return final_graph

        return graphe_de_flot, transformation(graphe_flot_max)
