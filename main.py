#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

from models.flowgraph import *
from ucagraphs import degrees_to_graph
from ucagraphs.flowing import *

graph = FlowingGraph(generate=figure_1, src_vertex='s', well_vertex='t')
flow, residual, flowing = graph.get_maximal_flow()
print(f"The flow of the first figure is {flow}")

sequence = [(3, 2), (3, 2), (1, 2), (1, 2)]
seq_flowing_graph, sequence_graph = degrees_to_graph(sequence)
seq_flowing_graph.plot(edge_labels=True)
sequence_graph.show()
