# UCA L3 - Optimisation des Graphes [TP]
> par **Rin Baudelet** et **Léna Combronde**
***
## Description 
Travaux pratiques d'optimisation des graphes avec pour objectif d'implémenter l'algorithme
d'Edmonds-Karp ainsi que la conversion d'une séquence en un problème de flots. 
> ### Dépendance
> ***
> **Python** version 3.9+ <br/>
> **SageMath** ~= 9.2 <br/>
> **Jupyter** Notebooks

## Organisation
- ### Sources files 
  - **(root)** <br/> contient le fichier main.py ainsi que les fichiers de configurations du projet
  - **/ucagraphs** <br/> contient les fonctions usuelles (@see \_\_init\_\_.py)
    - **/flowgraph** <br/> contient les différentes classes utiles pour le reste du 
    programme, dont notamment la partie sur Edmonds-Karp.
- ### Notebooks
> Contient notamment les notebooks jupyter utilisés pour les tests de fonctions et affichage des résultats
  - **edmonds\_karp.ipynb** contient les résultats de l'utilisation de l'algorithme d'Edmonds-Karp sur la première figure
  - **degrees\_sequence.ipynb** contient les résultats de la transformation de la sequence {(3,2),(3,2),(1,2),(1,2)} en problème de flots.
- ### Models
> contient l'implémentation des différentes figures nécessaires pour ces travaux.
- ### Latex
> contient le fichier source pour la génération du rapport de TP.